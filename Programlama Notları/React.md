# React <!-- omit in toc -->

> `HOME` tuşu ile yukarı yönlenebilrsiniz.

- [Neden react](#neden-react)
- [Faydalı Bağlantılar](#faydal%C4%B1-ba%C4%9Flant%C4%B1lar)

Facebook'un çıkarmış olduğu bir web programlama framework'udür.

## Neden react

- Sanal bir DOM oluşturarak DOM üzerinden değişiklik olduğunda tüm kod sanal DOM'a aktarılır ardından sadece değişen kısımları DOM'a aktarır.
- DOM'a sadece değişenler aktarıldıkları için daha hızlı ve daha senkronize işlem yapılır

## Faydalı Bağlantılar

- [Dökümantasyon](https://reactjs.org/docs/getting-started.html)
- [Ana Kavramlar](https://reactjs.org/docs/hello-world.html)
- [Hosting](https://www.roast.io/for/react)
- [Online IDE](https://codesandbox.io/s/new)