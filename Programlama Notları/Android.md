# Andorid

## Online Android Emulator

APKOnline adlı sitenin *emulator*'ü için [buraya](https://www.apkonline.net/free-android-online-emulator/run-android-online-emulator) bakabilirsin.

## Google Play Store

Uygulamlarınızı **google play store**'a yüklemek için *develeport* hesabı açmanı gerekmektedir.

- Google tek seferlik **25$**'lık bir ücret almaktadır
  - Hesabınızı kapatmanız durumunda bu ücret **iade** edilecektir
  - Hesabınızdaki uygulamaları başka hesaplara aktarmak için [buraya](https://play.google.com/apps/publish/?account=6847951054083969806#AppTransferPlace) bakabilirsin
- Uygulama satışlarının **%30**'u *Google*'a gitmektedir
