# Karma Notlar <!-- omit in toc -->

## İçerik <!-- omit in toc -->

> `HOME` tuşu ile yukarı yönlenebilrsiniz.

- [Faydalı Uygulamalar](#faydal%C4%B1-uygulamalar)
  - [Tasarım Uygulamaları](#tasar%C4%B1m-uygulamalar%C4%B1)
  - [Yönetim Uygulamaları & Siteleri](#y%C3%B6netim-uygulamalar%C4%B1--siteleri)
  - [Web programlama](#web-programlama)
- [Harici Bağlantılar](#harici-ba%C4%9Flant%C4%B1lar)

## Faydalı Uygulamalar

### Tasarım Uygulamaları

| Adı                                                                                    | Açıklama                                  |
| -------------------------------------------------------------------------------------- | ----------------------------------------- |
| [Figma](https://www.figma.com/) & [AdobeXD](https://www.adobe.com/tr/products/xd.html) | Uygulama görüntü tasarımı için kullanılır |

### Yönetim Uygulamaları & Siteleri

| Uygulama İsmi                                                | Açıklama                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Github](https://github.com/) & [Gitlab](https://gitlab.com) | Kaynak kod yöneticisi                                        |
| [Asana](https://asana.com/)                                  | Proje yönetimi & Yapılacaklar Aşaması & İş aktarımı / eşleme |
| [Slack](https://slack.com/)                                  | Takım yönetimi                                               |

### Web programlama

| Uygulama İsmi                                                            | Açıklama                                                         |
| ------------------------------------------------------------------------ | ---------------------------------------------------------------- |
| [Git](https://git-scm.com/downloads)                                     | Kaynak kod yönetimi                                              |
| [PhpStorm](https://www.jetbrains.com/phpstorm/download/#section=windows) | Çok fonksiyonel PHP IDE                                          |
| [Xammpp](https://www.apachefriends.org/tr/download.html)                 | Php için sunucu, veri tabanı vs. işlemleri sunan platform        |
| [Nodejs](https://nodejs.org/en/download/)                                | Javascript kodlarını makine koduna çevir. Js'i sunucuda kullanma |
| [MySQL](https://www.mysql.com/downloads/)                                | Veri tabanı yönetimi                                             |
| [Composer](https://getcomposer.org/download/)                            | Php paket yönetimi (NPM)                                         | Nodejs) gibi |

## Harici Bağlantılar

- [Google Sync Verisi Kaldırma](https://www.cnet.com/how-to/how-to-clear-google-chrome-sync-data/) | [Direkt Yönelendirme](https://chrome.google.com/sync)
- [Windows Sync Verisi Kaldırma](https://www.tenforums.com/tutorials/106159-delete-sync-settings-windows-10-devices-microsoft-account.html) | [Direkt Yönlendirme](https://onedrive.live.com/win8personalsettingsprivacy)