# Chrome <!-- omit in toc -->

> `HOME` tuşu ile yukarı yönlenebilrsiniz.

- [Chrome kısayolları](#chrome-k%C4%B1sayollar%C4%B1)
  - [Chrome sekme işlemleri](#chrome-sekme-i%C5%9Flemleri)
  - [Sayfa işlemleri](#sayfa-i%C5%9Flemleri)
  - [Geliştirici kısayolları](#geli%C5%9Ftirici-k%C4%B1sayollar%C4%B1)
- [Eklenti Önerileri](#eklenti-%C3%B6nerileri)
  - [Verimlilik Eklentileri](#verimlilik-eklentileri)
  - [Görsellik Eklentileri](#g%C3%B6rsellik-eklentileri)
  - [Sosyal Medya Eklentileri](#sosyal-medya-eklentileri)
  - [Programlama Eklentileri](#programlama-eklentileri)
- [Adress Çubuğu Anahtarları](#adress-%C3%A7ubu%C4%9Fu-anahtarlar%C4%B1)
- [Harici Linkler](#harici-linkler)

## Chrome kısayolları

### Chrome sekme işlemleri

| Kısayol                | Açıklama                   |
| ---------------------- | -------------------------- |
| `CTRL` + `T`           | Yeni sekme açma            |
| `CTRL` + `SHIFT` + `T` | Son kapatılan sekmeyi açma |
| `CTRL` + `W`           | Sekmeyi kapatma            |

### Sayfa işlemleri

| Kısayol                | Açıklama                                     |
| ---------------------- | -------------------------------------------- |
| `CTRL` + `R`           | Sayfayı yenileme                             |
| `CTRL` + `SHIFT` + `R` | Sayfayı komple yenileme (önbelleği temizler) |
| `CTRL` + `S`           | Sayfayı kaydetme                             |

### Geliştirici kısayolları

| Kısayol                | Açıklama                        |
| ---------------------- | ------------------------------- |
| `CTRL` + `SHIFT` + `I` | Dom konsolunu açma              |
| `CTRL` + `SHIFT` + `C` | HTML eleman seçiciyi aktif etme |

## Eklenti Önerileri

### Verimlilik Eklentileri

| Eklenti                                                                                                                           | Özelliği                                                                                                                              |
| --------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| [Adblock Plus](https://chrome.google.com/webstore/detail/adblock-plus/cfhdojbkjhnklbpkdaibdccddilifddb)                           | Reklam engelleme                                                                                                                      |
| [Pop up Blocker for Chrome](https://chrome.google.com/webstore/detail/pop-up-blocker-for-chrome/bkkbcggnhapdmkeljlodobbkopceiche) | Popup (Açılır pencere) engelleme *özeliikle film izleyenler için oldukça faydalı-                                                     |
| [Google Çeviri](https://chrome.google.com/webstore/detail/google-translate/aapbdbdomjkkjkaonfhkkikfgjllcleb)                      | Çeviri                                                                                                                                |
| [OneTab](https://chrome.google.com/webstore/detail/onetab/chphlpgkkbolifaimnlloiipkdnihall?hl=tr)                                 | Sekme karışıklığını ve fazla ram kullanımı engelleme                                                                                  |
| [Temp Mail](https://chrome.google.com/webstore/detail/temp-mail-disposable-temp/inojafojbhdpnehkhhfjalgjjobnhomj)                 | Geçici mail oluşturma eklentisi. 15s de mail onaylama imkanı sunuyor. *Özellikle kayıt ol diye baskılayan sitelere girenlere tavsiye- |

### Görsellik Eklentileri

| Eklenti                                                                                           | Açıklama       |
| ------------------------------------------------------------------------------------------------- | -------------- |
| [Lone Tree](https://chrome.google.com/webstore/detail/lone-tree/gdcbilggakcddojcadnfeckbpoomdmii) | Güzel bir tema |

### Sosyal Medya Eklentileri

| Eklenti                                                                                                                         | Açıklama                                                             |
| ------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------- |
| [App For WA Webb](https://chrome.google.com/webstore/detail/app-for-wa-web/bpocngoedbjmnmkngoohaccdmidcjjhm?hl=en)              | Whatsapp wep yerine oldukça kullanışlı basit açılır pencere          |
| [God Mode for Whatsapp](https://chrome.google.com/webstore/detail/god-mode-for-whatsapp/cgdfebhnckdgckcjhidjnochmahdohad)       | Whatsapp wep yerine oldukça kullanışlı ve ek özelliklere sahip popup |
| [Instagram Veri İndirici](https://chrome.google.com/webstore/detail/batch-media-saver-from-in/plmnmnpijgncjompjiccojbccinacefh) | Instagramdaki verilerini indiren eklenti                             |

### Programlama Eklentileri

| Eklenti                                                                                                     | Açıklama                                                     |
| ----------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| [Run Javascript](https://chrome.google.com/webstore/detail/run-javascript/lmilalhkkdhfieeienjbiicclobibjao) | Seçtiğiniz sayfalara girdiğinizde otomatik olarak JS derleme |

## Adress Çubuğu Anahtarları

Adres çubuğuna `chrome://settings/searchEngines` yazaeak gerekli ayarların olduğu kısma girin.

> Çıkan ekranda diğer arama motorları kısmında `Ekle` butonuna basın.

**Çıkan Menüde:**

- `Arama motoru ismi` *Kendi verdiğimiz isim*
- `Anahtar` *Adres çubuğuna yazınca alttaki sorguyu çalıştıracak*
- `Sorgu` URL veya sorgu URL olabilir size kalmış.

*Örnek Kullanım:*

- Tureng
- -t
- `http://tureng.com/tr/turkce-ingilizce/%s`

> %s sonrasındaki metin anlamına gelmekte. Örn; -t Help (*Help `%s` olan yere yazılır.*)

## Harici Linkler

- [Make Google Chrome 1000% Faster | Fix Memory](https://www.youtube.com/watch?v=6pjDn3m4rsU&list=PL1m1AtfGwsxmeK4bsX9IvcVS8jRvj0cly&index=2&t=0s)
- [Make Chrome Run Faster](https://www.techspot.com/article/1193-chrome-performance-memory-tweaks/)
- [The Power User's Guide to Chrome](https://lifehacker.com/the-power-users-guide-to-google-chrome-5045904)
- [Add Custom Keyboard Shortcuts to Chrome Extensions for Quick Launching](https://lifehacker.com/add-custom-keyboard-shortcuts-to-chrome-extensions-for-1595322121)