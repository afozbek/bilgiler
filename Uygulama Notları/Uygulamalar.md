# Uygulamalar <!-- omit in toc -->

## İçerikler <!-- omit in toc -->

- [Tarayıcı](#taray%C4%B1c%C4%B1)
- [Uzaktan Bağlantı](#uzaktan-ba%C4%9Flant%C4%B1)
- [Arşivleme](#ar%C5%9Fivleme)
- [Bilgisayar Bakımı](#bilgisayar-bak%C4%B1m%C4%B1)

## Tarayıcı

- Chrome
- Opera
- Firefox
- Vivaldi

## Uzaktan Bağlantı

- NoMachine
- TeamViewer

## Arşivleme

- WinRaR
- 7zip

## Bilgisayar Bakımı

- CCleaner
- Defraggler
- Stacer (linux)