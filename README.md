# Bilgiler <!-- omit in toc -->

![status](https://img.shields.io/nodeping/status/jkiwn052-ntpp-4lbb-8d45-ihew6d9ucoei.svg)
![quality](https://img.shields.io/ansible/quality/432.svg)
![made_with_Markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)
![licanse_mit](https://img.shields.io/github/license/mashape/apistatus.svg)

> Orjinal dosya [gitlab](https://gitlab.com/yedehrab/bilgiler) üzerindeki dosyadır.

## İçerikler <!-- omit in toc -->

> `HOME` tuşu ile yukarı yönlenebilrsiniz.

- [İşletim Sistemi Notları](#i%CC%87%C5%9Fletim-sistemi-notlar%C4%B1)
- [Yapay Zeka Notları](#yapay-zeka-notlar%C4%B1)
- [Uygulama Notları](#uygulama-notlar%C4%B1)
- [Proje Yönetimi Notları](#proje-y%C3%B6netimi-notlar%C4%B1)
- [Yazılım Notları](#yaz%C4%B1l%C4%B1m-notlar%C4%B1)
- [Teknolojik Alet Notları](#teknolojik-alet-notlar%C4%B1)
- [Karaca](#karaca)
- [Programlama Notları](#programlama-notlar%C4%B1)
- [Günlük Hayat Notları](#g%C3%BCnl%C3%BCk-hayat-notlar%C4%B1)
- [Ders Notları](#ders-notlar%C4%B1)
- [Harici Linkler](#harici-linkler)
- [Yapılacaklar](#yap%C4%B1lacaklar)
- [Çalışma Notları](#%C3%A7al%C4%B1%C5%9Fma-notlar%C4%B1)
- [Karma Bilgiler](#karma-bilgiler)
- [Lisans ve Teferruatlar](#lisans-ve-teferruatlar)

<!-- Index -->

## İşletim Sistemi Notları

- [Linux Notları](%C4%B0%C5%9Fletim%20Sistemi%20Notlar%C4%B1/Linux%20Notlar%C4%B1.md)
- [Windows 10 Notları](%C4%B0%C5%9Fletim%20Sistemi%20Notlar%C4%B1/Windows%2010%20Notlar%C4%B1.md)

## Yapay Zeka Notları

- [Yapay Zeka Kullanımı](Yapay%20Zeka%20Notlar%C4%B1/Yapay%20Zeka%20Kullan%C4%B1m%C4%B1.md)
- [Yapay Zeka Notları](Yapay%20Zeka%20Notlar%C4%B1/Yapay%20Zeka%20Notlar%C4%B1.md)
- [Veri Kümeleri](Yapay%20Zeka%20Notlar%C4%B1/Veri%20K%C3%BCmeleri.md)
- [Türkçe Yapay Zeka Kaynakları](Yapay%20Zeka%20Notlar%C4%B1/T%C3%BCrk%C3%A7e%20Yapay%20Zeka%20Kaynaklar%C4%B1.md)

## Uygulama Notları

- [LGSV Simulator](Uygulama%20Notlar%C4%B1/LGSV%20Simulator.md)
- [VsCode](Uygulama%20Notlar%C4%B1/VsCode.md)
- [JetBrains IDE](Uygulama%20Notlar%C4%B1/JetBrains%20IDE.md)
- [Chrome](Uygulama%20Notlar%C4%B1/Chrome.md)
- [Anaconda](Uygulama%20Notlar%C4%B1/Anaconda.md)
- [Karma Notlar](Uygulama%20Notlar%C4%B1/Karma%20Notlar.md)
- [Youtube](Uygulama%20Notlar%C4%B1/Youtube.md)

## Proje Yönetimi Notları

- [Git için Emojiler](Proje%20Y%C3%B6netimi%20Notlar%C4%B1/Git%20i%C3%A7in%20Emojiler.md)
- [Heroku Notları](Proje%20Y%C3%B6netimi%20Notlar%C4%B1/Heroku%20Notlar%C4%B1.md)
- [Proje Yönetimi](Proje%20Y%C3%B6netimi%20Notlar%C4%B1/Proje%20Y%C3%B6netimi.md)
- [Git Notları](Proje%20Y%C3%B6netimi%20Notlar%C4%B1/Git%20Notlar%C4%B1.md)

## Yazılım Notları

- [Terimler](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/Terimler.md)
- [Adb Komutları](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/Adb%20Komutlar%C4%B1.md)
- [Google Colabrotory](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/Google%20Colabrotory.md)
- [Google Hacking](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/Google%20Hacking.md)
- [Protocol Buffer (Protobuf)](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/Protocol%20Buffer%20%28Protobuf%29.md)
- [OpenCart](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/OpenCart.md)
- [GittiGidiyor Entegrasyon](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/GittiGidiyor%20Entegrasyon.md)
- [Karma Notlar](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/Karma%20Notlar.md)
- [Lisanslar](Yaz%C4%B1l%C4%B1m%20Notlar%C4%B1/Lisanslar.md)

## Teknolojik Alet Notları

- [Xiaomi Mi A2 Lite](Teknolojik%20Alet%20Notlar%C4%B1/Xiaomi%20Mi%20A2%20Lite.md)
- [HTC Desire 820](Teknolojik%20Alet%20Notlar%C4%B1/HTC%20Desire%20820.md)
- [Telefon Rehberini Taşıma](Teknolojik%20Alet%20Notlar%C4%B1/Telefon%20Rehberini%20Ta%C5%9F%C4%B1ma.md)
- [HP Bilgisayar Notları](Teknolojik%20Alet%20Notlar%C4%B1/HP%20Bilgisayar%20Notlar%C4%B1.md)

## Karaca

- [Kod Parçaları](Karaca/Kod%20Par%C3%A7alar%C4%B1.md)
- [Tasarım](Karaca/Tasar%C4%B1m.md)
- [Faydalı Eklentirler](Karaca/Faydal%C4%B1%20Eklentirler.md)

## Programlama Notları

- [Markdown](Programlama%20Notlar%C4%B1/Markdown.md)
- [Web Programlama](Programlama%20Notlar%C4%B1/Web%20Programlama.md)
- [DOM](Programlama%20Notlar%C4%B1/DOM.md)
- [Python](Programlama%20Notlar%C4%B1/Python.md)
- [React](Programlama%20Notlar%C4%B1/React.md)
- [MySQL](Programlama%20Notlar%C4%B1/MySQL.md)
- [Nodejs Çok Kullanılan Kütüphaneler](Programlama%20Notlar%C4%B1/Nodejs%20%C3%87ok%20Kullan%C4%B1lan%20K%C3%BCt%C3%BCphaneler.md)
- [Php](Programlama%20Notlar%C4%B1/Php.md)
- [Javascript](Programlama%20Notlar%C4%B1/Javascript.md)
- [Nodejs](Programlama%20Notlar%C4%B1/Nodejs.md)
- [Genel Bilgiler](Programlama%20Notlar%C4%B1/Genel%20Bilgiler.md)

## Günlük Hayat Notları

- [Aydınlatma](G%C3%BCnl%C3%BCk%20Hayat%20Notlar%C4%B1/Ayd%C4%B1nlatma.md)
- [Çamaşır Makinesi](G%C3%BCnl%C3%BCk%20Hayat%20Notlar%C4%B1/%C3%87ama%C5%9F%C4%B1r%20Makinesi.md)

## Ders Notları

- [Bilgisayar Mimarisi](Ders%20Notlar%C4%B1/Bilgisayar%20Mimarisi.md)
- [İş Sağlığı ve Güvenliği](Ders%20Notlar%C4%B1/%C4%B0%C5%9F%20Sa%C4%9Fl%C4%B1%C4%9F%C4%B1%20ve%20G%C3%BCvenli%C4%9Fi.md)
- [Software Enginnering](Ders%20Notlar%C4%B1/Software%20Enginnering.md)
- [Computer Networks and Technologies](Ders%20Notlar%C4%B1/Computer%20Networks%20and%20Technologies.md)
- [Web Programlama](Ders%20Notlar%C4%B1/Web%20Programlama.md)
- [Görüntü İşleme](Ders%20Notlar%C4%B1/G%C3%B6r%C3%BCnt%C3%BC%20%C4%B0%C5%9Fleme.md)
- [Bilgisayar Grafikleri](Ders%20Notlar%C4%B1/Bilgisayar%20Grafikleri.md)
- [Software Testing And Quality](Ders%20Notlar%C4%B1/Software%20Testing%20And%20Quality.md)

<!-- Index -->

## Harici Linkler

- [Alibaba Cloud for Students](https://www.alibabacloud.com/campaign/education)

## Yapılacaklar

- [x] Her sayfanın en üst ve en alt kısmına (?) işaretçi ekle.
- [x] Git notlarındaki komutları tablosal olarak göster.
- [x] win10 sık kullanılan cmd komutları tablosal olacak
- [x] Tensorflow kaynakları.ssd_inception_v2_coco.config açıklamalar eklenecek
- [ ] 🌐📃📺 Döküman yapısı eklenecek
- [ ] Tablolara ☆ favori özellği eklenecek
- [ ] Konu ve kaynakları için dizinler oluşturalaca
  - [ ] Ders Notları/Software Enginnering/README.md
  - [ ] Ders Notları/Software Enginnering/res
  - [ ] Bölümlere ayrılabilir, göz korkutan bilgileri engeller.

## Çalışma Notları

- Önce sabit değerli yaz, sonrasında `CTRL`+ `F` ile sabitleri değişken haline dönüştür
- Terimler *italik*, önemli notlar **bold**, komutlar `code`, matematikler (latex) $latex$ şekilde olmalı.

## Karma Bilgiler

- [The Best Font for Resume](https://www.businessnewsdaily.com/5331-best-resume-fonts.html)

## Lisans ve Teferruatlar

Bu yazı **MIT** lisanslıdır. Lisanslar hakkında bilgi almak için [buraya](https://choosealicense.com/licenses/) bakmanda fayda var.

- [Website](https://yemreak.com)
- [Github](https://github.com/yedehrab)
- [GitLab](https://gitlab.com/yedehrab)

> Yardım veya destek için [iletişime](mailto::yedhrab@gmail.com) geçebilrsiniz 🤗

~ Yunus Emre Ak