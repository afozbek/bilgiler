# HTC Desire 820 <!-- omit in toc -->

HTC Desire 820 için karma hata çözümleri

## İçerikler <!-- omit in toc -->

> `HOME` tuşu ile yukarı yönlenebilrsiniz.

- [Sim Kartını Okumama Sorunu](#sim-kart%C4%B1n%C4%B1-okumama-sorunu)
- [Harici Bağlantılar](#harici-ba%C4%9Flant%C4%B1lar)

## Sim Kartını Okumama Sorunu

- Arama yerini açıp
- `*#*#4636#*#*` yazın
  - Otomatik olarak sizi ayarlar alanına atacaktır
- `Telefon Bilgileri`
- Biraz aşağı kaydırdığınızda
  - Tercih edilen ağ türünü belirle kısmında
  - `WCDMA preffered` yazan kısmı `GSM/WCDMA, CDMA, and EvDo (PRL)` yapın

## Harici Bağlantılar

- [HTC Sim Card Not Showing Problem FIXED](https://www.youtube.com/watch?v=tayD5NVgug8)