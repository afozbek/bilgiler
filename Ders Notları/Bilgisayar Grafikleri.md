# Bilgisayar Grafikileri

## Ders İçeriği

Ders içerikleri drive üzerinden yedeklenmektedir, [buraya](https://drive.google.com/open?id=1IXqAliNEU8XS3UN0uqi8MwnadANph_Pz) tıklayarak erişebilirsin.

## Vize Hakkında

1) 3 nokta verilmisti bu noktalar ile olusan cismi (4,2) noktasina gore 45 derece dondurup y eksenine gore yansitmamizi istedi

2) 7 nokta icin jacobien degerleri

3) 4 noktasi ve 4 noktadaki turevi verilmis cubic spline sorusu vardi 2.  Egrideki 1/3 degerini bulmamizi istiyordu fakat sonuc hesaplamaniza gerek yok matris carpimi yeterli dedi

4) Program sorusu olarakta 4 nokta verip bu noktalarla olusan bezier egrisinin programini yazmamizi istedi

Sorularin hepsi esit puanliydi