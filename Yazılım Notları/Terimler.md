# Terimler

Yazılımda kullanılan genel terimler.

| Terim        | Türkçe Karşılığı | Ek Açıklama                                                                                       |
| ------------ | ---------------- | ------------------------------------------------------------------------------------------------- |
| Feed         | Akış             | Instagramdaki resim alanı, veya sitelerdeki ana verilerin alanı                                   |
| Feature      | Özellik          |                                                                                                   |
| Bug          | Hata - Sıkıntı   | Yazılımın açılmaması gibi çeşitli sorunlar                                                        |
| Dev          | Geliştirici      |                                                                                                   |
| Script       | Dile özgü kod    | Belli bir de yazılan proje kadar iyi olmayan kod topluluğu                                        |
| Code Snipped | Kod Parçası      | 1-2 satırlık kodlardan oluşan kod parçası                                                         |
| Register     | Yazmaç           |                                                                                                   |
| Cache        | Önbellek         | Verileri hafızada tutup hızlı açmak için önbellek kullanılır                                      |
| Cookie       | Çerez            | Bir siteye tekrardan girdiğimizde giriş bilgilerimiz gibi bilgileri koruması, çerezlerle sağlanır |
| Run          | Çalıştırma       | Yazılan kodu derleyici üzerinde çalıştırma                                                        |
| Debug        | Hata Ayıklama    | Kodu derleyici üzerinde adım adım gerekli yerlerde duracak şekilde çıktılarla çalıştırma          |
| Banner       | Afiş             |                                                                                                   |
| Slider       | Kayan Afiş       |
| Namespace    | İsim alanı       | Aynı amaca hizmet eden özellikleri, sınıfları ve fonksiyonları aynı çatı altında toplama          |
| Wild Card    |                  | `..` `.` `*` gibi terimleri içeren metne verilen isim                                             |