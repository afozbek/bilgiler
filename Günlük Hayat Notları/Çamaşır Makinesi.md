# Çamaşır Makinesi <!-- omit in toc -->

Çamaşır makinesi ile alakası olmayanlar için 😄

> Hotpoint Ariston baz alınmıştır.

## İçerik <!-- omit in toc -->

- [Yıkama Öncesi Notlar](#y%C4%B1kama-%C3%B6ncesi-notlar)
- [Programlar](#programlar)
  - [Yıkama Programları](#y%C4%B1kama-programlar%C4%B1)
  - [Kurutma Programları](#kurutma-programlar%C4%B1)
  - [Diğer Programlar](#di%C4%9Fer-programlar)
- [Yıkama Sonrası Notlar](#y%C4%B1kama-sonras%C4%B1-notlar)
- [Harici Linkler](#harici-linkler)

## Yıkama Öncesi Notlar

- 1 çay bardağı deterjanı yıkama gözüne koyun
- 0.5 çay bardağı deterjanı ön yıkama gözüne koyun

> Sıvı deterjan eklenecekse özel kapağın kapatılması gerekmekte

## Programlar

### Yıkama Programları

| Program            | Derece | Devir | Yaklaşık Saat |
| ------------------ | ------ | ----- | ------------- |
| Koyu Renkliler     | 30°    | 800   | 1.20          |
| Renkliler (Yoğun)  | 40     | 1400  | 2:55          |
| Çok Renkliler      | 20     | 140   | 3:05          |
| Sentetik           | 60     | 100   | 2:05          |
| Pamuklu            | 60     | 1400  | 3:35          |
| Eko Pamuklu        | 60     | 1400  | 3:45          |
| Karışık            | 30     | 800   | 0:30          |
| Yünlü              | 40     | 800   | 1:40          |
| Çok Narinler       | 30     | Yok   | 1:20          |
| Anti Alerjiler     | 40     | 1400  | 3:50          |
| Anti Alerji Ekstra | 60     | 1400  | 3:30          |

### Kurutma Programları

| Program  | Derece | Devir | Yaklaşık Saat |
| -------- | ------ | ----- | ------------- |
| Yünlü    | Yok    | Yok   | 2:40          |
| Sentetik | Yok    | Yok   | 4:15          |
| Pamuk    | Yok    | Yok   | 4:10          |

### Diğer Programlar

| Program  | Derece | Devir | Yaklaşık Saat |
| -------- | ------ | ----- | ------------- |
| Sıkma    | Yok    | 1400  | 0:10          |
| Durulama | Yok    | 1400  | 0:49          |

## Yıkama Sonrası Notlar

- Yıkandığı gibi çamaşır toplanır
  - Islak kalan çamaşırlar küflenir pis kokar
- Silkelenerek petek veya benzeri sıcak yerlere asılır
  - Silkelenmezse kuruduktan sonra buruş buruş olur ütü gerektirir

## Harici Linkler

- [Çamaşır Yıkamanın Püf Noktaları](https://www.cokbilgi.com/yazi/camasir-yikamanin-puf-noktalari-pratik-bilgiler/)